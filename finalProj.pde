import processing.sound.*;


FFT fft;
AudioIn in;
Visualizer vis;
Features feat;
Generator G;
ControlP5 cp5;
controlP5.Button play_b, mnist_b, abst_b;
controlP5.Slider speed;
int bins = 8;
int scene = 0;
boolean show_inter = true;
float[] w = new float[bins];
controlP5.Slider[] freqs = new controlP5.Slider[bins];

Menu M;
int bands = 512;

AudioStream as; 
void setup() {
  // Create an Input stream which is routed into the Amplitude analyzer
  fft = new FFT(this, bands);
  in = new AudioIn(this, 0);
  background(24);
  size(512, 512);
  cp5 = new ControlP5(this);
  as = new AudioStream(0);
  vis = new Visualizer();
  G = new Generator("mnist");
  M = new Menu();


  play_b = cp5.addButton("Play")
    .setPosition(width/4, height/2)
    .setSize(50, 50);
  
  mnist_b = cp5.addButton("MNIST")
    .setPosition((width-50), (height*0.4)-50)
    .setSize(50, 50);
  
  abst_b = cp5.addButton("ABSTRACT")
    .setPosition((width-50), (height*0.6)-50)
    .setSize(50, 50);
    
  abst_b = cp5.addButton("CIFAR")
    .setPosition((width-50), (height*0.5)-50)
    .setSize(50, 50);


  speed = cp5.addSlider("Speed")
    .setPosition(width-100, 50)
    .setSize(100, 14)
    .setScrollSensitivity(1.1)
    .setRange(0, 1)
    .setValue(0.2)
    ;

  for (int i=0; i < bins; i++) {
    freqs[i] = cp5.addSlider("Freq:"+i)
      .setPosition(i*(width/bins), 0)
      .setSize((width/bins), 40)
      .setScrollSensitivity(1.1)
      .setRange(0, 100)
      .setValue(50)
      ;
  }
}

void weighfreq() {
  int counter = 0;
  for (controlP5.Slider slide : freqs) {
    feat.weights[counter] = slide.getValue();
    counter++;
  }
}


void Play() {
  print("play");
  scene = 1;
  play_b.hide();
}

void MNIST() {
  print("MNIST");
  G = new Generator("mnist");
}
void ABSTRACT() {
  print("ABSTRACT");
  G = new Generator("abst");
}
void CIFAR() {
  print("CIFAR");
  G = new Generator("cifar");
}

void draw() {
  if (key == 'h') {
    show_inter = false;
  }
  if (key == 's') {
    show_inter = true;
  }
  background(24);
  if (scene == 0)
  {
    M.display();
  }
  if (scene == 1) {
    weighfreq();
    as.stream();
    feat.calcFeat();
    weighfreq(); 
    G.update();
    G.sensitivity = speed.getValue();
    G.display();
    if (show_inter) {
      cp5.show();
      vis.display();
    } else {
      cp5.hide();
    }
  }
}
