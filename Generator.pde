import ai.onnxruntime.NodeInfo;
import ai.onnxruntime.OnnxTensor;
import ai.onnxruntime.OrtEnvironment;
import ai.onnxruntime.OrtException;
import ai.onnxruntime.OrtSession;
import ai.onnxruntime.OrtSession.Result;
import java.nio.FloatBuffer.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;


class Generator {
  int steps;
  OrtEnvironment env;
  OrtSession session;
  String inputName;
  float[][][][] sourceArray;
  PImage frame;
  float[] jitters;
  float[] update_dir;
  String model;
  int img_size;
  int z_size;
  float sensitivity = 0.2;
  


  Generator( String model) {
    this.model = model;
    
    
    feat = new Features(as, 100,bins);
    String name = "generator.onnx";
    if (model == "abst") {
      this.img_size = 64;
      this.z_size = 128;
      feat = new Features(as, z_size,bins);
      name = "bla.onnx";
      sourceArray = new float[1][128][1][1];
      jitters = new float[128];
      update_dir = new float[128];
    }
    
    if (model == "mnist") {
      this.img_size = 28;
      this.z_size = 100;
      feat = new Features(as, z_size,bins);
      name = "mnist.onnx";
      sourceArray = new float[1][this.z_size][1][1];
      jitters = new float[this.z_size];
      update_dir = new float[this.z_size];
    }
    
    if (model == "cifar") {
      this.img_size = 32;
      this.z_size = 100;
      feat = new Features(as, z_size,bins);
      name = "generator.onnx";
      sourceArray = new float[1][this.z_size][1][1];
      jitters = new float[this.z_size];
      update_dir = new float[this.z_size];
    }

    this.frame = createImage(img_size, img_size, RGB);
    env = OrtEnvironment.getEnvironment();
    try {
      session = env.createSession("/Users/jonaskraasch/Documents/Processing/finalProj/"+name, new OrtSession.SessionOptions());
      inputName = session.getInputNames().iterator().next();
    }
    catch(OrtException e) {
      throw new RuntimeException(e);
    }

    for (int i=0; i<z_size; i++) {
      sourceArray[0][i][0][0] = randomGaussian();
    }

    for (int i=0; i<z_size; i++) {
      update_dir[i] = random(1)>0.5?1:-1;
    }
  }

  // get new jitters jitter == 0.5
  void new_jitters(float jitter) {

    for (int i=0; i<this.z_size; i++) {
      jitters[i] = random(1)<0.5?1:1-jitter;
    }
  }

 
  
  //get new update directions
  float[] new_update_dir(float[][][][] noise) {
    int counter = 0;
    float n;
    for (float[][] n_vec : noise[0]) {   
      n = n_vec[0][0];

      if (n >= (2 - 0.25)) { //0.25 == tempo_sensitivity
        update_dir[counter] = -1;
      } else {
        if (n < (-2 + 0.25)) {
          update_dir[counter] = 1;
        }
      }

      counter++;
    }
    return update_dir;
  }



  void update() {
    float[] update_dir = new_update_dir(sourceArray);
    float[] gradm = {0.1, 0.1, 0.1, 0.1};
    float[] specm; 
    float update=0, spec_feat=0, grad_feat = 0;

    if (steps%200==0) {
      println("jitter----");
      new_jitters(0.5);
    }

    // update = np.array([tempo_sensitivity for k in range(128)]) * (gradm[i]+specm[i]) * update_dir * jitters 



    gradm = feat.grad_buffer[(feat.grad_id+1)%2];
    specm = feat.mean_buffer[(feat.mean_id+1)%2];
    
    for (float mean : specm) {
      spec_feat += mean; 
    }
    spec_feat /= specm.length;

    for (float grad : gradm) {
      grad_feat += grad;
    }

    grad_feat /= gradm.length;
    //print(grad_feat, " ");

    for (int i=0; i<this.z_size; i++) {
      update =  sensitivity * 10*(spec_feat+grad_feat) * update_dir[i] * jitters[i];
      //println("Update:", update, update_dir[i]);
      sourceArray[0][i][0][0] += update;
    }
    steps++;
  }
  

  void display() {
    try {
      OnnxTensor tensorFromArray = OnnxTensor.createTensor(env, sourceArray);
      Result output = session.run(Collections.singletonMap(inputName, tensorFromArray));
      float[][][][] image =  (float[][][][])output.get(0).getValue();

      frame.loadPixels();
      for (int k=0; k<image[0][0][0].length; k++) {
        for (int j=0; j<image[0][0].length; j++) {
          float c0, c1, c2;
          if (image[0].length == 3) {
            c0 = (image[0][0][k][j]+1)*127.5;
            c1 = (image[0][1][k][j]+1)*127.5;
            c2 = (image[0][2][k][j]+1)*127.5;
          } else {
            c0 = (image[0][0][k][j]+1)*127.5;
            c1 = (image[0][0][k][j]+1)*127.5;
            c2 = (image[0][0][k][j]+1)*127.5;
          }

          frame.pixels[(k*image[0][0][0].length)+j] = color(c0, c1, c2);
        }
      }
      frame.updatePixels();
      image(frame, 0, 0, width, height);
    }

    catch(OrtException e) {
      throw new RuntimeException(e);
    }
  }
}
