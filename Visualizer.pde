class Visualizer {

  int mode = 0;
  void display() {

    float[] mel = (feat.feature);
    
    int counter = 0;
    int weight_id = 0;
    counter = 0;
    float max=0;
    for (int i = 0; i < mel.length; i++) {
      // The result of the FFT is normalized
      // draw the line for frequency band i scaling it up by 5 to get more amplitude.
      if(counter>(mel.length/bins)){
      
      //println(weight_id,"MAX", max);
      weight_id++;
      counter=0;
      max = 0;
      }
      stroke(189);
      if(mel[i]> max) max = mel[i];
      line( i, height, i, height - (mel[i]*feat.weights[weight_id])*height);
      counter++;
    }
    //println(weight_id,"MAX", max);
    counter = 0;
    fill(200,20,20);
    for(float mean: feat.mean_buffer[feat.mean_id]){
      circle((counter*(width/bins))+((width/bins)/2),height-(mean*height), 10);
      counter++;
    }
  }
}
