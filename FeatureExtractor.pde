import java.util.Arrays;
import java.util.DoubleSummaryStatistics;


class Features {
  float[] feature;
  float[] direction;
  float[][] mean_buffer;
  float[][] grad_buffer;
  float[] weights;
  int mean_id = 0;
  int grad_id = 0;
  int bins;
  AudioStream audio;
  


  Features(AudioStream audio, int f_size, int bins) {
    this.bins = bins;
    this.audio = audio;
    this.feature = new float[f_size];
    this.direction = new float[f_size];
    this.mean_buffer = new float[2][bins];
    this.grad_buffer = new float[2][bins];
    weights = new float[bins];
    for (int i=0; i<feature.length; i++) {
      feature[i] = randomGaussian();
    }
    
    for(int i=0; i<bins; i++){
      weights[i] = 1;
    }


    for (int i=0; i<feature.length; i++) {
      float rand = random(0, 1);
      if (rand<0.5) {
        direction[i] = -1;
      } else {
        direction[i] = 1;
      }
    }
  }



  double[] convertFloatsToDoubles(float[] input)
  {
    if (input == null)
    {
      return null; // Or throw an exception - your choice
    }
    double[] output = new double[input.length];
    for (int i = 0; i < input.length; i++)
    {
      output[i] = input[i];
    }
    return output;
  }


  
  
  float[] normalize(float[] arr) {
    float[] norm = new float[arr.length];
    float min = arr[0];
    float max = arr[0];
    for (float val : arr) {
      if (min > val) {
        min = val;
      }
      if (max < val) {
        max = val;
      }
    }

    int id = 0;
    if ((max-min)!=0) {
      for (float num : arr) {
        norm[id] = (num-min)/(max-min);
        id++;
      }
    }
    return norm;
  }
  // Calculates the base-10 logarithm of a number
  float log10 (float x) {
    return (log(x) / log(10));
  }

  float[] toMel(float[] freq) {
    float[] bla = new float[freq.length];
    for (int i=0; i < freq.length; i++) {
      bla[i] = 2595*log10((1+(abs(freq[i])/700)));
    }
    return bla;
  }

  void compute_mean() {
    compute_mean(audio.spectrum);
  }

  void compute_mean(float[] freq) {
    float[] mean = new float[this.bins];

    float[] bin = new float[bands/this.bins];
    
    for (int range=0; range<this.bins; range++) {
      mean[range] = 0;
      for (int j=0; j<bands/this.bins; j++) {
        bin[j] = freq[j];
      }
      //bin = normalize(bin);
      for (float val : bin) {
        mean[range]+=val;
      }
      mean[range] /= (bands/this.bins);
      mean[range] *= weights[range];
    }
    mean_buffer[mean_id] = mean;
    mean_id = (mean_id+1)%2;
  }


  void compute_grad() {

    float[] grad = new float[this.bins];
    float comp_grad;

    for (int range=0; range<this.bins; range++) {
      comp_grad =(mean_buffer[0][range] - mean_buffer[1][range]);
      grad[range] = comp_grad;//>0?comp_grad:0;
    }
    this.grad_buffer[grad_id] = grad;
    grad_id = (grad_id+1) % 2;
  }


  void randomFeat() {

    for (int i=0; i<this.bins; i++) {
      this.grad_buffer[0][i] = randomGaussian();
      this.grad_buffer[1][i] = randomGaussian();
      this.mean_buffer[0][i] = randomGaussian();
      this.mean_buffer[1][i] = randomGaussian();
      
    }
  }
  void calcFeat() {
    float[] spec = toMel(audio.spectrum);
    compute_mean(spec);
    compute_grad();
    feature = spec;
  }
}
